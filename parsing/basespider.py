#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from urlparse import urlparse
from scrapy import signals
from scrapy import Spider, Selector, Request
from scrapy.xlib.pydispatch import dispatcher

import io
import json
import datetime

class BaseSpider(Spider):
    start_urls = ['']
    country = ''
    currency = ''

    def __init__(self):
        dispatcher.connect(self.spider_closed, signals.spider_closed)
        super(BaseSpider, self).__init__()
        self.domain = '{url.scheme}://{url.netloc}'.format(url=urlparse(self.start_urls[0]))
        self.data = list()

    def spider_closed(self):
        """
        Write data to file
        """
        with io.open('/tmp/{}.json'.format(self.name), 'w', encoding='utf8') as fp:
            fp.write(unicode(json.dumps(self.data, sort_keys=True, ensure_ascii=False)))

    def convert_body(self, response):
        """
        Parse site body
        """
        # Get site body text
        site_body = response.body
        if isinstance(site_body, str):
            site_body = site_body.decode('utf-8')
        return site_body

    def create_station_data(self, response):
        station_data = dict()
        station_data['url'] = response.url
        station_data['country'] = self.country
        station_data['currency'] = self.currency
        station_data['date'] = datetime.datetime.now().strftime('%F %T')
        station_data['fuels'] = list()
        return station_data