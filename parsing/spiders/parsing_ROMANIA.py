# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from parsing.basespider import BaseSpider
from scrapy import Spider, Selector, Request
from scrapy.utils.markup import remove_tags
from slugify import slugify

class RomaniaSpider(BaseSpider):
    name = 'stations_RO'
    start_urls = ['http://www.gazonline.ro/cautare.php?jud_cod=0']
    base_url = 'http://www.gazonline.ro/statie_detalii.php?st_id={0}'
    country = 'RO'
    currency = 'RON'

    def __init__(self):
        super(RomaniaSpider, self).__init__()

    def parse(self, response):
        for i in xrange(7, 2533):
            yield Request(
                self.base_url.format(i), callback=self.parse_gas_station, dont_filter=True)

    def parse_gas_station(self, response):
        """
        Parse gas station page
        """
        # Get site body text
        site_body = response.body.decode(response.encoding)

        # Get gas stations body
        selector = Selector(text=site_body)

        # Check if the station is jailed
        if selector.xpath('//td[contains(text(), "Statie  inchisa")]'):
            return

        station_data = self.create_station_data(response)

        temp = selector.xpath('//table[contains(@class, "tbl_chenar")][count(tr)=4]')
        temp2 = temp.css('td[class=txt_body]').extract_first()

        if temp2 is None:
            return

        # Selector of gas station
        station_data['address'] = remove_tags(temp2, ('td', 'br'))
        station_data['name'] = ' '.join(
            remove_tags(selector.css('td[class=det_titlu]').extract_first(), ('td', 'a', 'img')).split()
        )
        node_geo = remove_tags(
            selector.xpath('.//td[contains(.//text(), "Latitudine")]').extract_first(), ('td', 'img')
        ).split()
        station_data['latitude'] = node_geo[2]
        station_data['longitude'] = node_geo[-1]

        # Get fuel info
        if selector.xpath('//table[@class="tbl_pret"][count(tr)=2]'):
            fuels_selector = selector.xpath(
                '//table[@class="tbl_pret"][count(tr)=2]')[0]
            for i, fuel in enumerate(fuels_selector.css('.td_cap')):
                station_data['fuels'].append(
                    {
                        'name': slugify(remove_tags(fuel.extract(), ('td', 'a', 'img', 'br')).strip()),
                        'price': float(fuels_selector.css('.td_bg1')[i].css('.txt_pret_act::text').extract_first()),
                        'last_updated': fuels_selector.css('.td_bg1')[i].css('.txt_small::text')[1].extract().strip()
                    }
                )
            self.data.append(station_data)