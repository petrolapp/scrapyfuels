# -*- coding: utf-8 -*-
from urlparse import urlparse
from scrapy import Spider, Selector, Request
from scrapy import signals
from parsing.basespider import BaseSpider
from scrapy.xlib.pydispatch import dispatcher

import json
import datetime


class MylpgSpider(BaseSpider):
    name = 'stations_MYLPG'
    start_urls = ['http://www.mylpg.eu/stations/']
    base_url = 'http://www.mylpg.eu/stations/{0}/list'
    
    countries_map = {}
    countries_map['albania'] = 'AL'
    countries_map['austria'] = 'AT'
    countries_map['belarus'] = 'BY'
    countries_map['bosnia-and-herzegovina'] = 'BA'
    countries_map['bulgaria'] = 'BG'
    countries_map['croatia'] = 'HR'
    countries_map['czech-republic'] = 'CZ'
    countries_map['denmark'] = 'DK'
    countries_map['estonia'] = 'EE'
    countries_map['france'] = 'FR'
    countries_map['finland'] = 'FI'
    countries_map['germany'] = 'DE'
    countries_map['greece'] = 'GR'
    countries_map['hungary'] = 'HU'
    countries_map['ireland'] = 'IE'
    countries_map['san-marino'] = 'SM'
    countries_map['italy'] = 'IT'
    countries_map['kosovo'] = 'RS'
    countries_map['latvia'] = 'LV'
    countries_map['liechtenstein'] = 'LI'
    countries_map['lithuania'] = 'LT'
    countries_map['luxembourg'] = 'LU'
    countries_map['macedonia'] = 'MK'
    countries_map['malta'] = 'MT'
    countries_map['montenegro'] = 'ME'
    countries_map['netherlands'] = 'NL'
    countries_map['norway'] = 'NO'
    countries_map['poland'] = 'PL'
    countries_map['portugal'] = 'PT'
    countries_map['romania'] = 'RO'
    countries_map['russia'] = 'RU'
    countries_map['serbia'] = 'RS'
    countries_map['slovakia'] = 'SK'
    countries_map['slovenia'] = 'SI'
    countries_map['spain'] = 'ES'
    countries_map['sweden'] = 'SE'
    countries_map['switzerland'] = 'CH'
    countries_map['turkey'] = 'TR'
    countries_map['ukraine'] = 'UA'
    countries_map['united-kingdom'] = 'GB'
    countries_map['united-states-of-america'] = 'US'
    
    sites = {}
    
    def __init__(self):
        super(MylpgSpider, self).__init__()

    def parse(self, response):
        
        source =self.convert_body(response)
        selector = Selector(text=source)        
        countries_strings = selector.xpath('//*[@class="list-stations"]/tr/td[1]/a/@href').extract()
        countries = []
        
        for country in countries_strings:            
            yield Request(self.base_url.format(country.split('/')[-1]), callback=self.parse_stations_list, dont_filter=True)
#        yield Request(self.base_url.format('estonia'), callback=self.parse_stations_list, dont_filter=True)


            
    def parse_stations_list(self, response):
        country =  self.countries_map.get(response.url.split('/')[-2],  None)        
        self.currency = None
        
        
        source =self.convert_body(response)        
        selector = Selector(text=source)
        
        rows = selector.xpath('//*[@id="table-search"]/tbody/tr').extract()
        
        for row in rows:            
            
            row_selector = Selector(text=row)
            link = row_selector.xpath('//td[1]/a/@href').extract_first()
            
            
            request =  Request(link, callback=self.parse_station, dont_filter=True)
            request.meta['proxy']='http://178.49.228.101:3128'
            request.meta['country']=country
            yield request        
        
    def parse_station(self,  response):
        country = response.meta.get('country',  None)
        currency = None
        station = self.create_station_data(response)      
        source =self.convert_body(response)        
        selector = Selector(text=source)
        
        name = selector.xpath('//*[@class="post"]/div/div[2]/div[2]/table/tr[2]//text()').extract_first()
        address_strings = selector.xpath('//*[@class="post"]/div/div[2]/div[2]/table/tr[5]/td/div//text()').extract()
        url = selector.xpath('//*[@class="post"]/div/div[2]/div[2]/table/tr[8]//text()').extract_first()
        self.sites[url] = country
        lat = float(selector.xpath('//*[@class="post"]/div/div[2]/div[3]/table/tr[11]/td/div/meta/@content').extract()[0])
        long = float(selector.xpath('//*[@class="post"]/div/div[2]/div[3]/table/tr[11]/td/div/meta/@content').extract()[1])
        price_str = selector.xpath('//*[@class="post"]/div/div/div[2]/div[2]/p[3]//text()').extract()[1]
        try:
            last_update = selector.xpath('//*[@class="post"]/div/div/div[2]/div[2]/p[3]//text()').extract()[2].split(' ')[1]
        except:
            None  
        
        price = None
        unit = None
        try:
            price = float(price_str.split(' ')[0].strip())
            unit =  price_str.split(' ')[1].split('/')[1]            
            currency =  price_str.split(' ')[1].split('/')[0]
        except:
            None       
        
        address = ''
        for str in address_strings:
            str = str.strip()
            address += str + ' '
        address = address.strip()
        
        
        station['name'] = name
        station['address'] = address
        station['latitude'] = lat
        station['longitude'] = long
        station['currency'] = currency
        station['country'] = country
        
        fuels = []
        fuel = {}
        fuel['name'] = 'LPG'
        fuel['price'] = price
        fuel['unit'] = unit
        fuel['last_updated'] = last_update
        fuels.append(fuel)
        station['fuels'] = fuels
        self.data.append(station)       
        
     

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    
