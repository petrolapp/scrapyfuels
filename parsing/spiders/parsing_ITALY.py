# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from parsing.basespider import BaseSpider
from scrapy import Spider, Selector, Request
from slugify import slugify

class ItalySpider(BaseSpider):
    name = 'stations_IT'
    start_urls = ['http://www2.prezzibenzina.it']
    base_url = 'http://www2.prezzibenzina.it/distributori/{0}'
    country = 'IT'
    currency = 'EUR'

    def __init__(self):
        super(ItalySpider, self).__init__()

    def parse(self, response):
        """
        Parse site content
        """
        for i in range(0, 31700):
            yield Request(self.base_url.format(i), callback=self.parse_gas_station, dont_filter=True)

    def parse_gas_station(self, response):
        """
        Parse gas station site content
        """
        station_data = self.create_station_data(response)

        # Get site body text
        site_body = response.body.decode(response.encoding)

        # Selector of gas station
        gas_station_selector = Selector(text=site_body)
        if not gas_station_selector.css('div[class=st_address]::text').extract_first():
            return

        node_geo = gas_station_selector.css('img[class=aerial_pic]::attr(src)').extract_first().split('=')[1].split(',')
        print node_geo
        station_data['latitude'] = node_geo[0]
        station_data['longitude'] = node_geo[1].split('&')[0]
        station_data['address'] = '{}, {}'.format(
            gas_station_selector.css('div[class=st_city]::text').extract_first(),
            gas_station_selector.css('div[class=st_address]::text').extract_first()
        )
        station_data['name'] = gas_station_selector.xpath('.//div[@class="st_name"]/a/text()').extract_first()

        # Get fuel info
        for fuel in gas_station_selector.css('div[class=st_reports_row]')[1:]:
            price = fuel.css('div[class=st_reports_price]::text').extract_first()
            if not price:
                continue
            station_data['fuels'].append({
                'name': slugify(fuel.css('div[class*=st_reports_fuel]::text').extract_first()),
                'price': float(price.split()[0]),
                'type': fuel.css('div[class=st_reports_service]::text').extract_first(),
                'last_updated': fuel.css('div[class=st_reports_data]::text').extract_first()
            })
        self.data.append(station_data)