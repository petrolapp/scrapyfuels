# -*- coding: utf-8 -*-
from urlparse import urlparse
from scrapy import Spider, Selector, Request
from scrapy import signals
from parsing.basespider import BaseSpider
from scrapy.xlib.pydispatch import dispatcher

import json
import datetime

class SerbiaSpider(BaseSpider):
    name = 'stations_SI_RS_HR_BA_ME'
    start_urls = ['http://www.petrol.eu/gas-stations/list']
    country = None
    currency = 'EUR'

    def __init__(self):
        super(SerbiaSpider, self).__init__()

    def parse(self, response):
        source =self.convert_body(response)
        selector = Selector(text=source)
        petrols_list = selector.xpath('//*[@class="views-field views-field-title"]/a/@href').extract()
        for request in petrols_list:
            yield Request(url='http://www.petrol.eu/'+request, callback=self.parse_station, dont_filter=True)

    def parse_station(self, response):
        station = self.create_station_data(response)
        source = self.convert_body(response)
        xhs = Selector(text=source)
        country = xhs.xpath('//*[@class="views-row views-row-1 views-row-odd views-row-first views-row-last"]/span/a/text()').extract()[0]

        if country.find('Slovenia') != -1:
            station['country'] = 'SI'
            station['currency'] = 'EUR'
        elif country.find('Croatia') != -1 or country.find('Dalmatia') != -1 or country.find('kotar') != -1:
            station['country'] = 'HR'
            station['currency'] = 'HRK'
        elif country.find('Bosnia and Herzegovina') != -1:
            station['country'] = 'BA'
            station['currency'] = 'BAM'
        elif country.find('Kosovo') != -1:
            station['country'] = 'XK'
            station['currency'] = 'EUR'
        elif country.find('Montenegro') != -1:
            station['country'] = 'ME'
            station['currency'] = 'EUR'
        elif country.find('Serbia') != -1:
            station['country'] = 'RS'
            station['currency'] = 'RSD'
        else:
            station['country'] = None
            station['currency'] = None

        station['name'] = xhs.xpath('//*[@class="page-title"]/text()').extract()[0]
        coord = xhs.xpath('//*[@class="qr-code-popup"]/p/text()').extract()
        try:
            station['latitude']  = float(coord[0].split(' ')[1])
            station['longitude']   = float(coord[1].split(' ')[1])
        except ValueError:
            None

        units = xhs.xpath('//*[@class="cf gas-price-content"]/dl/dd/span/text()').extract()
        fuels = xhs.xpath('//*[@class="cf gas-price-content"]/dl/dd/text()').extract()
        for i in range(0,  len(fuels)/2):
            fuel = {}
            fuel['name'] = fuels[i*2]
            try:
                fuel['price'] = float(fuels[i*2+1].strip().replace(',',  '.'))
            except ValueError:
                None
            fuel['unit'] = units[i].split('/')[1]
            station['fuels'].append(fuel)
        address = xhs.xpath('//*[@class="address"]/text()').extract()[0]
        station['address'] = address 
        self.data.append(station)