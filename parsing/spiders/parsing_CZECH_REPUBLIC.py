# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from parsing.basespider import BaseSpider
from scrapy import Spider, Selector, Request

class CzechRepublicSpider(BaseSpider):
    name = 'stations_CZ'
    start_urls = ['http://www.mbenzin.cz/Ceny-benzinu-a-nafty']
    base_url = 'http://www.mbenzin.cz/Ceny-benzinu-a-nafty---CS_{0}'
    country = 'CZ'
    currency = 'CZK'

    def __init__(self):
        super(CzechRepublicSpider, self).__init__()

    def parse(self, response):
        for i in xrange(16202,19512):
            yield Request(self.base_url.format(i), callback=self.parse_gas_station, dont_filter=True)

    def parse_gas_station(self, response):
        station_data = self.create_station_data(response)

        # Get site body text
        site_body = self.convert_body(response)

        # Selector of gas station
        gas_station_selector = Selector(text=site_body)

        station_data['name'] = gas_station_selector.css('span[id=content_lName]::text').extract_first()
        if station_data['name'] is None:
            return

        station_data['address'] = '{0}, {1}'.format(
            gas_station_selector.css('a[id=content_hlCity]::text').extract_first(),
            gas_station_selector.css('span[id=content_lStreet]::text').extract_first()
        )

        geo = response.xpath('//script/text()')[-1].extract().split('"position":{')[1].split('}')[0].split(',')
        # print geo
        station_data['latitude'] = float(geo[0].split(':')[1])
        station_data['longitude'] = float(geo[1].split(':')[1])

        gas_station_info = gas_station_selector.css('table')
        # Get fuel info
        for fuel in gas_station_info.css('tr'):
            fuel = Selector(text=fuel.extract())
            spans = fuel.xpath('//span/text()').extract()
            #print spans
            station_data['fuels'].append({
                'name': spans[0].strip(),
                'price': float(spans[1]),
                'last_updated': spans[2]
            })
        if len(station_data['fuels']) > 0:
            self.data.append(station_data)