# -*- coding: utf-8 -*-
from urlparse import urlparse
from scrapy import Spider, Selector, Request
from scrapy import signals
from parsing.basespider import BaseSpider
from scrapy.xlib.pydispatch import dispatcher

import json
import datetime

class GazpromSpider(BaseSpider):
    name = 'stations_GAZPROM'
    start_urls = ['http://www.gpncard.ru/en/map_of_stations/list_of_stations/?SHOWALL_2=1']

    def __init__(self):
        super(GazpromSpider, self).__init__()

    def parse(self, response):
        source =self.convert_body(response)
        selector = Selector(text=source)
        regions = selector.xpath('//*[@class="region_label"]/text()').extract()
        region_stations = selector.xpath('//*[@class="stations_list"]').extract()        
        
        currency = ''
        country = ''

        for i in range(0, len(regions)):
            if regions[i].find(u'Республика Беларусь') != -1:                
                country = 'BY'
                currency = 'BYN'
            elif regions[i].find(u'Республика Польша') != -1:                
                country = 'PL'
                currency = 'PLN'
            elif regions[i].find(u'Республика Таджикистан') != -1:                
                country = 'TJ'
                currency = 'TJS'
            elif regions[i].find(u'Республика Украина') != -1:                
                country = 'UA'
                currency = 'UAH'
            else:
                country = 'RU'
                currency = 'RUB'
            stations_selector = Selector(text=region_stations[i])
            stations = stations_selector .xpath('//li').extract()            
            for j in range(0, len(stations)):                
                station = self.create_station_data(response)
                station['country'] = country
                station['currency'] = currency
                station_sel = Selector(text=stations[j])
                address = station_sel.xpath('//*[@class="address"]/text()').extract_first()
                name = station_sel.xpath('//*[@class="azs_name_link"]/b/text()').extract_first()
                station['name'] = name
                station['address'] = address 
                
                try:
                    lat = station_sel.css('a[href="#"]::attr(rel)').extract_first().split('_')[0]
                    long = station_sel.css('a[href="#"]::attr(rel)').extract_first().split('_')[1]                
                    
                    station['latitude']  = float(lat)
                    station['longitude']   = float(long)
                except AttributeError:
                    None
                
                fuels = station_sel.xpath('//*[@class="info"]/table[1]/tr/td').extract()                
                station['fuels'] = []           
                for f in range(1,  len(fuels)):                    
                    try:
                        fuel = {}
                        
                        fuel_sel = Selector(text=fuels[f])
                        price = fuel_sel.xpath('//*[@class="price"]/text()').extract_first().strip().replace(',',  '.')                    
                        fuel['price'] = float(price)
                        name = fuel_sel.css('img[alt=""]::attr(src)').extract_first()
                        name = name[name.rfind('/fuel_')+1:].replace('_s.png',  '')
                        
                        fuel['name'] = name
                        station['fuels'].append(fuel)
                    except AttributeError:
                        None
                self.data.append(station)                
        return
