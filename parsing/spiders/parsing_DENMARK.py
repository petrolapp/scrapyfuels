# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from parsing.basespider import BaseSpider
from scrapy import Spider, Selector, Request
from scrapy.utils.markup import remove_tags
from slugify import slugify

class DenmarkSpider(BaseSpider):
    name = 'stations_DK'
    start_urls = ['http://www.fdmbenzinpriser.dk/listepriser']
    country = 'DK'
    currency = 'DKK'

    def __init__(self):
        super(DenmarkSpider, self).__init__()

    def parse(self, response):
        """
        Parse site content
        """
        # Get site body text
        site_body = self.convert_body(response)

        # Use XPath
        selector = Selector(text=site_body)

        # Get list company
        fuel_selector = selector.xpath('//div[@class="companyprices"]')[0]
        for fuel_mark in fuel_selector.css('div[class=rows]'):
            yield Request('{0}{1}'.format(self.domain, fuel_mark.css('a::attr(href)').extract_first()),
                callback=self.parse_fuel_mark, dont_filter=True)

    def parse_fuel_mark(self, response):
        # Get site body text
        site_body = self.convert_body(response)

        # Selector of gas station
        fuel_mark_selector = Selector(text=site_body)
        for station in fuel_mark_selector.css('div[class=stations]').css('a'):
            yield Request('{0}{1}'.format(self.domain, station.css('a::attr(href)').extract_first()),
                callback=self.parse_gas_station, dont_filter=True)

    def parse_gas_station(self, response):
        station_data = self.create_station_data(response)
        geo = response.xpath('//img[@itemprop="image"]/@src')[0].extract().split('&sensor=')[0].split('|')[-1].split(',')
        #print geo
        station_data['latitude'] = float(geo[0])
        station_data['longitude'] = float(geo[1])

        # Get site body text
        site_body = self.convert_body(response)

        # Selector of gas station
        gas_station_selector = Selector(text=site_body)
        station_data['address'] = ' '.join(remove_tags(
            gas_station_selector.css('div[itemprop=address]').extract_first(), ('span', 'div')).split())
        station_data['name'] = gas_station_selector.css('div[itemprop=name]::text').extract_first()

        # Get fuel info
        for fuel in gas_station_selector.css('div[class=singleprice]'):
            price = fuel.css('div[class=price]::text').extract_first()
            if price == '-.-':
                continue
            station_data['fuels'].append({
                'name': slugify(fuel.css('div[itemprop=name]::text').extract_first()),
                'price': float(price)
            })
        self.data.append(station_data)