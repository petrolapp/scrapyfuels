#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from parsing.basespider import BaseSpider
from scrapy import Spider, Selector, Request
from scrapy.utils.markup import remove_tags
from slugify import slugify

class BulgariaSpider(BaseSpider):
    name = 'stations_BG'
    start_urls = ['https://fuelo.net/?lang=en']
    base_url = 'https://fuelo.net/gasstation/id/{0}?lang=en'
    country = 'BG'
    currency = 'EUR'

    def __init__(self):
        super(BulgariaSpider, self).__init__()
        print self.name

    def parse(self, response):
        for i in xrange(1,40609):
            yield Request(self.base_url.format(i), callback=self.parse_gas_station, dont_filter=True)

    def parse_gas_station(self, response):
        station_data = self.create_station_data(response)
        station = response.xpath('//div[@class="promoted-caption"]/h1/text()').extract_first()
        station_data['name'] = station.strip()
        address = response.xpath('//div[@class="promoted-caption"]/h2/text()').extract_first()
        station_data['address'] = address.strip()
        address_details = response.xpath('//div[@class="col-sm-12 col-md-4"]/h4')
        station_data['country'] = address_details[0].xpath('img/@src').extract_first().split('/')[-1].split('.')[0].upper()
        geo = response.xpath('//meta[@property="place:location:latitude"]/@content')
        station_data['latitude'] = float(geo[0].extract())
        geo = response.xpath('//meta[@property="place:location:longitude"]/@content')
        station_data['longitude'] = float(geo[0].extract())

        # Get site body text
        site_body = self.convert_body(response)

        # Selector of gas station
        gas_station_selector = Selector(text=site_body)
        gas_station_info = gas_station_selector.css('table')

        # Get fuel info
        for fuel in gas_station_info.css('tr')[1:]:
            fuel = Selector(text=fuel.extract())
            price = fuel.xpath('//span[@itemprop="price"]/@content').extract()
            if len(price) == 0:
                continue
            station_data['currency'] = fuel.xpath('//span[@itemprop="priceCurrency"]/@content').extract_first().upper()
            station_data['fuels'].append(
                {
                    'name': fuel.xpath('//img/@src').extract_first().split('/')[-1].split('.')[0],
                    'price': float(price[0])
                }
            )
        self.data.append(station_data)