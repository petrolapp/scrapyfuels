# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import requests
import scrapy
from parsing.settings import PARSE_ERROR
import datetime
import time

FUEL_TYPES = {
    u'Benzin 95': 'fuel_a95',
    u'Nafta': 'fuel_diesel',
    u'LPG': 'fuel_autogas',
    u'Benzin 98': 'fuel_a98',
    u'Premium Benzin': 'fuel_a98',
}

CURRENCY = 'SFR'


class AntonioZucchiSpider(scrapy.Spider):
    name = 'benzin_SK'
    start_urls = ['http://www.benzin.sk/index.php?selected_id=163&article_id=-1&kraj_id=-1&okres_id=-1&obec_id=-1&brand_id=6&pump_id=169']

    def parse(self, response):
        """
        Parse site content

        """
        pass



