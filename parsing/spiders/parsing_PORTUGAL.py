# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime
import scrapy
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from parsing.settings import DCAP
from parsing.settings import PARSE_ERROR
import time
import re


FUEL_TYPES = {
    u'95': 'fuel_a95',
    u'Gasolina 95': 'fuel_a95',
    u'Super Plus': 'fuel_a95',
    u'95 S': 'fuel_a95sp',
    u'95S': 'fuel_a95sp',
    u'Gasolina simples 95': 'fuel_a95sp',
    u'Gasolina 98': 'fuel_a98',
    u'G': 'fuel_diesel',
    u'Gasóleo': 'fuel_diesel',
    u'Gas\xf3leo': 'fuel_diesel',
    u'Gasóleo Super': 'fuel_dieselsp',
    u'Gas\xf3leo simples': 'fuel_dieselsp',
    u'G S': 'fuel_dieselsp',
    u'GS': 'fuel_dieselsp',
    u'GPL Auto': 'fuel_autogas',
    u'GPL': 'fuel_autogas'
}


CURRENCY = 'EUR'


class CasteloBrancoSpider(scrapy.Spider):
    name = 'castelo_branco_PT'
    start_urls = ['http://www.kuantokusta.pt/auto-moto/Postos/J-LOURENCO-CASTELO-BRANCO']

    def parse(self, response):
        """
        Parse site content
        :return
        {'status': True, u'fuel_a95sp': {'date': u'27 de Outubro de 2015', 'price': u'1,239'},
        'name': u'J. LOUREN\xc7O - CASTELO BRANCO',
        'url': u'http://www.kuantokusta.pt/auto-moto/Postos/J-LOURENCO-CASTELO-BRANCO',
        u'fuel_autogas': {'date': u'27 de Outubro de 2015', 'price': u'0,710'}, 'longitude': u'-7.51844',
        'currency': u'\u20ac', u'fuel_diesel': {'date': u'27 de Outubro de 2015', 'price': u'1,039'},
        u'fuel_dieselsp': {'date': u'27 de Outubro de 2015', 'price': u'1,019'},
        u'fuel_a95': {'date': u'27 de Outubro de 2015', 'price': u'1,259'},
        'address': u'Zona Industrial lote K, 6001-459 Castelo Branco', 'latitude': u'39.80789'}
        """
        # Default fuel data
        fuel_currency = None
        st_url = response.url

        # Default fuel data
        fuel_data = dict(status=True, url=st_url, currency=CURRENCY, addresses=dict())

        # Setup driver
        driver = webdriver.PhantomJS('phantomjs', service_log_path=None, desired_capabilities=DCAP)
        driver.set_window_size(1024, 768)

        # Get site content request
        driver.get(response.url)

        # Add time delay for download and load page
        driver.implicitly_wait(3)

        try:
            # Default publication date
            pub_date = None

            # Get name
            st_name_node = driver.find_element_by_xpath(".//p[@class='postos-name']")
            st_name = st_name_node.text

            # Get address
            st_address_node = driver.find_element_by_xpath(".//p[@class='postos-address']")
            st_address = st_address_node.text.replace('\n', ', ')

            # Setup address key
            fuel_data['addresses'][st_address] = {'name': st_name}

            # Get GEO data
            geo_data_nodes = driver.find_elements_by_xpath(".//div[contains(@class, 'page-postos-lat-lon')]/div")
            st_latitude = geo_data_nodes[0].find_element_by_xpath("span").text
            st_longitude = geo_data_nodes[1].find_element_by_xpath("span").text

            # Get fuel nodes
            fuel_data_nodes = driver.find_elements_by_xpath(".//div[@class='page-postos-map-tipo-combustiveis']/div")

            for node in fuel_data_nodes:
                # get fuel type
                fuel_name_raw = node.find_element_by_xpath("div[@class='sigla']/span").text
                fuel_type = FUEL_TYPES.get(fuel_name_raw, fuel_name_raw)

                if not fuel_type:
                    fuel_type = u'fuel_autogas'

                # Get fuel price and currency
                fuel_price_patt = re.compile(r'(?P<price>[0-9\.,]+)(?P<currency>[^0-9]*)')
                fuel_price_raw = node.find_element_by_xpath("div[@class='valor_tipo']/span").text
                res = fuel_price_patt.search(fuel_price_raw)
                if res:
                    fuel_price = res.group('price')
                    if not fuel_currency:
                        fuel_currency = res.group('currency')
                else:
                    fuel_price = None

                if not pub_date:
                    date_nodes = driver.find_elements_by_xpath(
                        ".//div[@class='page-postos-services']/div[contains(@class, 'page-postos-services-update')]")
                    pub_date_raw = date_nodes[1].text
                    data_patt = re.compile(r'[^0-9]*(?P<date>[0-9].*)$')
                    res = data_patt.search(pub_date_raw)
                    if res:
                        pub_date = res.group('date')
                    else:
                        pub_date = datetime.datetime.now().strftime('%F %T')

                # Update fuel type data dict
                fuel_data['addresses'][st_address].update({fuel_type: dict(price=fuel_price, pub_date=pub_date)})

            # Add currency
            if fuel_currency:
                fuel_data['currency'] = fuel_currency

            # Add GEO data
            fuel_data['addresses'][st_address].update({'longitude': st_longitude, 'latitude': st_latitude})

        except Exception:
            fuel_data['status'] = False

        finally:
            driver.close()

        yield fuel_data


class CompareomercadoSpider(scrapy.Spider):
    start_urls = ['https://www.compareomercado.pt/preco_combustiveis/estacao-servico/galp-oeiras-cascais-lisboa']
    name = 'compareomercado_PT'

    def parse(self, response):
        """
        Parse site content
        """

        # GEO params
        st_longitude = None
        st_latitude = None

        # Default fuel data
        fuel_currency = None
        st_url = response.url

        # Default pub_date
        pub_date = datetime.datetime.now().strftime("%F %T")

        # Default fuel data
        fuel_data = dict(status=True, url=st_url, currency=CURRENCY, addresses=dict())

        # Setup selector
        selector = scrapy.Selector(text=response.body.decode(response.encoding))

        try:
            # Get main station info block
            st_block = selector.xpath(".//div[@class='petrol-station-results']/div[1]")

            # Get name and st_address
            st_name = st_block.xpath(".//h3/text()").re('[^-]+')[0].strip()
            st_city = st_block.xpath(".//h3/text()").re('[^-]+')[1].strip()

            # Get info blocks
            info_blocks = st_block.xpath("div[2]/div")

            # Get address
            st_address_raw_lst = info_blocks[1].xpath("p/text()").extract()
            st_address = '%s, %s, %s' % (st_city.capitalize(), st_address_raw_lst[1], st_address_raw_lst[0])

            # Setup address key
            fuel_data['addresses'][st_address] = {'name': st_name}

            # Get fuel nodes
            fuel_data_nodes = info_blocks.xpath(".//p[contains(text(), '\u20ac')]/..")

            for node in fuel_data_nodes:
                # get fuel type
                fuel_name_raw = node.xpath("h6/text()").extract_first().replace(':', '')
                fuel_type = FUEL_TYPES.get(fuel_name_raw, fuel_name_raw)

                # Get fuel price and currency
                fuel_price = node.xpath("p/text()").re('[0-9,]+')[0]

                if not fuel_currency:
                    fuel_currency = node.xpath("p/text()").re('[^0-9,]+')[0]

                # Update fuel type data dict
                fuel_data['addresses'][st_address].update({fuel_type: dict(price=fuel_price, pub_date=pub_date)})

            # Add currency
            if fuel_currency:
                fuel_data['currency'] = fuel_currency

            # Add GEO data
            fuel_data['addresses'][st_address].update({'longitude': st_longitude, 'latitude': st_latitude})

        except Exception:
            fuel_data['status'] = False

        yield fuel_data


class PrecoscombustiveisSpider(scrapy.Spider):
    start_urls = ['http://www.precoscombustiveis.dgeg.pt']
    name = 'precoscombustiveis_PT'

    def parse(self, response):

        def _parse_fuel_blocks(list_blocks):
            """
            Parse fuel type, price and pub_date.
            """
            res_data = dict(currency=CURRENCY)
            #
            fuel_currency = None

            # GEO params
            st_longitude = None
            st_latitude = None

            # Setup price pattern
            price_patt = re.compile(r'(?P<price>[0-9\.]+)(?P<curr>.+)')

            id_delay = 1

            try:
                for row_block in list_blocks:
                    if id_delay == 10:
                        time.sleep(300)

                    # Get st_info
                    st_info_blocks = row_block.find_elements_by_xpath("td[@class='Celula']")

                    extra_info_elm = row_block.find_element_by_xpath('*//div[@class="divIcon"]/a')

                    # Get st_name
                    st_name = st_info_blocks[0].text

                    # Get extra address info
                    extra_info_elm.click()
                    time.sleep(3)

                    # Return block with text address (u'Morada\nAv. Dr. Germano Vieiran\xba 559\n4470-050 Maia - Gueifaes')
                    extra_info_blk = row_block.find_elements_by_xpath("following-sibling::tr[@class='tdOwner']")[0]

                    # Get raw data
                    raw_data_text = extra_info_blk.find_elements_by_xpath("*//*[@class[contains(., 'infoPostos')]]//div")[0].text
                    list_raw_data = raw_data_text.split('\n')[1:]

                    # Setup address
                    st_address = ', '.join(list_raw_data)

                    # Setup address key
                    res_data[st_address] = {'name': st_name}

                    # Get fuel price
                    fuel_price_data = price_patt.search(st_info_blocks[3].text)

                    if fuel_price_data:
                        fuel_price = fuel_price_data.group('price')
                    else:
                        fuel_price = None

                    if not fuel_currency:
                        fuel_currency = fuel_price_data.group('curr')

                    # Get pub date
                    pub_date = st_info_blocks[6].text

                    # Get fuel type
                    fuel_type = FUEL_TYPES.get(st_info_blocks[5].text, st_info_blocks[5].text)

                    # Init res_data
                    res_data[st_address] = res_data.get(st_address, dict())

                    # Add fuel data
                    res_data[st_address].update({'name': st_name,
                                                 fuel_type: dict(price=fuel_price, pub_date=pub_date)})

                    # Add GEO data
                    res_data[st_address].update({'longitude': st_longitude, 'latitude': st_latitude})

                    # Add delay
                    time.sleep(12)

                    # Fix IIS error (401 - Unauthorized: Access is denied due to invalid credentials)
                    id_delay += 1

                # Add currency
                if fuel_currency:
                    res_data['currency'] = fuel_currency

            except Exception:
                pass

            finally:
                return res_data

        # Default fuel data
        fuel_currency = None
        st_url = response.url

        # Default fuel data
        fuel_data = dict(status=True, url=st_url, currency=CURRENCY, addresses=dict())

        # Setup driver
        driver = webdriver.PhantomJS('phantomjs', service_log_path=None, desired_capabilities=DCAP)
        # driver = webdriver.Firefox()
        # driver.set_window_size(1024, 768)

        # Get site content request
        driver.get(response.url)

        try:
            driver.switch_to.frame(driver.find_elements_by_tag_name("frame")[0])

            # Get all fuel option
            fuel_option = dict()
            all_fuel_option = driver.find_elements_by_xpath(".//*[@id='tipoCombustivel']/option")

            for option in all_fuel_option:
                fuel_option[option.get_attribute('value')] = option.text

            # Get fuel select element
            element = driver.find_element_by_id("tipoCombustivel")

            # Parse fuels
            for fuel_id in list(fuel_option.keys()):
                # print("PARSE FUEL_TYPE: {}: Value is: {} !!!!".format(fuel_id, FUEL_TYPES.get(fuel_option[fuel_id])))

                # Get option with this fuel_type
                sel_element = element.find_element_by_xpath("option[@value='{}']".format(fuel_id))

                # Select this option
                sel_element.click()

                # Get fuel search button element
                search_btn_element = driver.find_element_by_xpath(".//*[@class='conteudo']/*[@class='procurar']/a")

                # Press search
                search_btn_element.send_keys(Keys.RETURN)
                driver.implicitly_wait(6)

                # Find all rows for this select option
                fuel_data_rows = driver.find_elements_by_xpath(
                    ".//table[@class='TabelaGeral']//tr[@class[contains(., 'trPar')] and position()>1]")

                if fuel_data_rows:
                    parse_res = _parse_fuel_blocks(fuel_data_rows)

                    # Add st_data to main dict
                    if not fuel_currency:
                        fuel_data['currency'] = parse_res.pop('currency')
                    else:
                        del parse_res['currency']

                    # Save fuel data
                    fuel_data['addresses'].update(parse_res)

                # Add time delay for download and reload page
                driver.implicitly_wait(12)
                driver.get(response.url)

                # Switch to search frame
                driver.switch_to.frame(driver.find_elements_by_tag_name("frame")[0])

                # Get fuel select element
                element = driver.find_element_by_id("tipoCombustivel")

        except Exception:
            pass
        finally:
            driver.close()

        yield fuel_data





