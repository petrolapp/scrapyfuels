# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from urlparse import urlparse
from scrapy import Spider, Selector, Request
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
from slugify import slugify
from parsing.basespider import BaseSpider
import json

class MdLukoilSpider(BaseSpider):
    name = 'stations_MD_LUKOIL'
    start_urls = ['http://www.lukoil.md/en/main']
    base_url = 'https://www.google.com/maps/d/viewer?ll=46.950262%2C28.234863&spn=3.374904%2C4.224243&hl=en&t=m&msa=0&z=8&source=embed&ie=UTF8&mid=1gozXi_XKaIn3g4e-AA1d0WgTGj4'
    domain = '{url.scheme}://{url.netloc}'.format(url=urlparse(start_urls[0]))
    country = 'MD'
    currency = 'MDL'
    data = list()
    fuels = []

    def __init__(self):
        dispatcher.connect(self.spider_closed, signals.spider_closed)
        super(MdLukoilSpider, self).__init__()

    def parse(self, response):        
        source =self.convert_body(response)
        selector = Selector(text=source)
        petrols = selector.xpath('(//*[@class="priceblock_en "])/div').extract()
        
        
        for p in petrols:
            fuel = {}
            fuel_selector = Selector(text=p)            
            fuel['price'] = float(fuel_selector.xpath('(//*[@class="price"])/text()').extract_first())
            name =  fuel_selector.xpath('(//*[@class="pricetype"])/text()').extract_first()
            if name == None:
                name =  fuel_selector.xpath('(//*[@class="pricename"])/text()').extract_first()            
            fuel['name'] = name
            self.fuels.append(fuel)

        yield Request(url=self.base_url, callback=self.parse_stations, dont_filter=True)

    def parse_stations(self, response):
        stations = response.body.decode('UTF-8').split('[[[\\\"KYW2CDKF4MXU07B00EAEF5000001\\\"')[1].split('data')[0].split('[[[')
        stations.pop(0)
        stations.pop()
        for st in stations:
            station = self.create_station_data(response)
            try:
                station['latitude'] = float(st.split(']')[0].split(',')[0])
                station['longitude'] = float(st.split(']')[0].split(',')[1])
                station['name'] = st.split(',[\\\"')[1].split('\\\"')[0].replace('\\n',  '')
                station['address'] = st.split(',[\\\"')[3].split('\\\"')[0].replace('\\n',  '')                
                station['petrols'] = self.fuels
            except:
                continue
            self.data.append(station)        
        
                
            
