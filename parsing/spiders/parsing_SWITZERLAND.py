# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from parsing.basespider import BaseSpider
from scrapy import Spider, Selector, Request, FormRequest
from slugify import slugify

class SwitzerlandSpider(BaseSpider):
    name = 'stations_CH'
    start_urls = 'http://www.benzin-preis.ch/suche-kanton'
    country = 'CH'
    currency = 'CHF'

    def __init__(self):
        super(SwitzerlandSpider, self).__init__()

    def start_requests(self):
        """
        Override base method for send form request
        """
        return [
            FormRequest(self.start_urls,
                formdata={
                    'kanton': '%', 'was': 'suche-kanton', 's1': 'blf',
                    's2': 'leer', 's3': 'leer', 'alter': '9999',
                    'waehrung': 'Dollar', 'search': 'suchen'
                }, callback=self.parse)
        ]

    def parse(self, response):
        """
        Parse site content
        """
        # Get site body text
        site_body = response.body.decode(response.encoding)
        selector = Selector(text=site_body)
        # Get list gas stations
        for station in selector.xpath('//div[@class="block-zeile"]/a/@href'):
            url = '{0}/{1}'.format(self.domain, station.extract())
            yield Request(
                url, callback=self.parse_gas_station, dont_filter=True,
                meta={
                    'splash': {
                        'endpoint': 'render.html',
                        'args': {'wait': 3}
                    }
                }
            )
        pages = [
            int(link.extract().split('=')[1]) for link in selector.xpath(
                '//div[@id="menu"]/a/@href')
            ]
        for i in xrange(pages[0], pages[1], 5):
            url = '{}?next={}'.format(response.url, i)
            yield Request(
                url, callback=self.pasrse_list_gas_stations, dont_filter=True
            )

    def pasrse_list_gas_stations(self, response):
        # Get site body text
        site_body = response.body.decode(response.encoding)
        selector = Selector(text=site_body)
        # Get list gas stations
        for station in selector.xpath('//div[@class="block-zeile"]/a/@href'):
            url = '{0}/{1}'.format(self.domain, station.extract())
            yield Request(
                url, callback=self.parse_gas_station, dont_filter=True,
                meta={
                    'splash': {
                        'endpoint': 'render.html',
                        'args': {'wait': 3}
                    }
                }
            )

    def parse_gas_station(self, response):
        """
        Parse gas station site content
        """
        station_data = self.create_station_data(response)
        station_data['url'] = response.meta['_splash_processed']['args']['url']

        site_body = self.convert_body(response)
        selector = Selector(text=site_body)

        name =selector.css('h1[class=franchise]::text').extract_first()
        if name is None:
            return

        station_data['latitude'] = None
        station_data['longitude'] = None
        geo_pre = site_body.split("google.maps.LatLng(")
        if len(geo_pre) > 1:
            geo = geo_pre[1].split(')')[0].split(',')
            station_data['latitude'] = float(geo[0])
            station_data['longitude'] = float(geo[1])

        # Selector of gas station
        station_data['name'] = name.strip()
        station_data['address'] = selector.css('h2[class=franchise]::text').extract_first().strip()

        # Get fuel info
        for fuel in selector.css('div[class=block-zeile]'):
            station_data['fuels'].append({
                'name': slugify(fuel.css('div[class=art]::text').extract_first()),
                'price': fuel.css('div[class=preis]::text').extract_first()
            })
        self.data.append(station_data)