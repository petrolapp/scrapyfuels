# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from urlparse import urlparse
from scrapy import Spider, Selector, Request
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
from slugify import slugify
from parsing.basespider import BaseSpider
import json

class ArisSpider(BaseSpider):
    name = 'stations_ARIS'
    start_urls = ['https://www.ariscard.com/en/maps/countries/at/#panel2']
    base_url = 'https://www.ariscard.com'
    domain = '{url.scheme}://{url.netloc}'.format(url=urlparse(start_urls[0]))
    country = 'ARIS'
    currency = 'EUR'
    data = list()
    
    name_path = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[3]//text()'
    date_paths = {}
    date_paths['at'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[8]/text()'
    date_paths['be'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[8]/text()'
    date_paths['de'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[8]/text()'
    date_paths['ee'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[8]/text()'
    date_paths['es'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[8]/text()'
    date_paths['frn'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[8]/text()'
    date_paths['lt'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[8]/text()'
    date_paths['lu'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[8]/text()'
    date_paths['lv'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[8]/text()'
    date_paths['nl'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[8]/text()'
    date_paths['slo'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[8]/text()'
    date_paths['sk'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[8]/text()'
    date_paths['bg'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[9]/text()'
    date_paths['bh'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[9]/text()'
    date_paths['by'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[9]/text()'
    date_paths['hr'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[9]/text()'
    date_paths['hu'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[9]/text()'
    date_paths['kg'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[9]/text()'
    date_paths['kz'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[9]/text()'
    date_paths['pl'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[9]/text()'
    date_paths['ro'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[9]/text()'
    date_paths['rs'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[9]/text()'
    date_paths['ru'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[9]/text()'
    date_paths['se'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[9]/text()'
    date_paths['tj'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[9]/text()'
    date_paths['ua'] = '(//*[@class="table table-striped"])[1]/tr[{0}]/td[9]/text()'
    
    country_codes = {}
    country_codes['at'] = 'AT'
    country_codes['be'] = 'BE'
    country_codes['bg'] = 'BG'
    country_codes['bh'] = 'BA'
    country_codes['by'] = 'BY'
    country_codes['de'] = 'DE'
    country_codes['ee'] = 'EE'
    country_codes['es'] = 'ES'
    country_codes['frn'] = 'FR'
    country_codes['hr'] = 'HR'
    country_codes['hu'] = 'HU'
    country_codes['kg'] = 'KG'
    country_codes['kz'] = 'KZ'
    country_codes['lt'] = 'LT'
    country_codes['lu'] = 'LU'
    country_codes['lv'] = 'LV'
    country_codes['nl'] = 'NL'
    country_codes['pl'] = 'PL'
    country_codes['ro'] = 'RO'
    country_codes['rs'] = 'RS'
    country_codes['ru'] = 'RU'
    country_codes['se'] = 'SE'
    country_codes['slo'] = 'SI'
    country_codes['sk'] = 'SK'
    country_codes['tj'] = 'TJ'
    country_codes['ua'] = 'UA'


    def __init__(self):
        dispatcher.connect(self.spider_closed, signals.spider_closed)
        super(ArisSpider, self).__init__()

    def parse(self, response):
        country = '/en/maps/countries/ua/'
        
        source =self.convert_body(response)
        selector = Selector(text=source)        
        countries = selector.xpath('(//*[@class="col-xs-12"])[3]/div/a/@href').extract()
        
        for country in countries:
            yield Request(url=self.base_url + country + '#panel2', callback=self.parse_stations, dont_filter=True)

    def parse_stations(self, response):
        country = response.url[-4:].replace('/',  '')       
        source = response.body
        
        try:
            source = source.decode('cp1251')
        except:
            None
        try:
            source = source.decode('utf-8')
        except:
            None
        try:
            source = source.decode('cp851')
        except:
            None
            
        xhs = Selector(text=source)        

        count = len(xhs.xpath('(//*[@class="table table-striped"])[1]/tr').extract())
        
        station = {}
        for i in range(3,  count):
            last_updated = xhs.xpath('(//*[@class="table table-striped"])[1]/tr[' + str(i) + ']/td[8]/text()').extract_first()            

            if last_updated == None:
                fuel = {}
                fuel['name'] = xhs.xpath('(//*[@class="table table-striped"])[1]/tr[' + str(i) + ']/td[1]/text()').extract_first()
                fuel['price'] = float(xhs.xpath('(//*[@class="table table-striped"])[1]/tr[' + str(i) + ']/td[2]/text()').extract_first().replace('/',  '').strip())
                fuel['last_updated'] = xhs.xpath('(//*[@class="table table-striped"])[1]/tr[' + str(i) + ']/td[4]/text()').extract_first().strip()
                station['fuels'].append(fuel)
            else:
                if station != {}:
                    self.data.append(station)
                
                station = self.create_station_data(response)
                station['country'] = self.country_codes[country]
                station['name'] = xhs.xpath(self.name_path.format(i)).extract_first()
                station['currency'] = xhs.xpath('(//*[@class="table table-striped"])[1]/tr[' + str(i) + ']/td[7]/text()').extract_first().strip()

                info = xhs.xpath('(//*[@class="table table-striped"])[1]/tr[' + str(i) + ']/td[4]/text()').extract()
                
                station['address'] = ''
                for ss in info:
                    if ss.find('GPS') != -1:
                        lat_str = ss.split(':')[1].strip().split(' ')[0]
                        long_str = ss.split(':')[1].strip().split(' ')[1]
                        lat_str = lat_str[0:-1]
                        long_str = lat_str[0:-1]
                        station['latitude'] = float(lat_str)
                        station['longitude'] = float(long_str)
                    elif ss.find('Open') == -1:
                        station['address'] += ss + ' '                
                
                fuel = {}
                try:
                    fuel['name'] = xhs.xpath('(//*[@class="table table-striped"])[1]/tr[' + str(i) + ']/td[5]/text()').extract_first()
                    fuel['price'] = float(xhs.xpath('(//*[@class="table table-striped"])[1]/tr[' + str(i) + ']/td[6]/text()').extract_first().replace('/',  '').strip())
                    fuel['last_updated'] = xhs.xpath(self.date_paths[country].format(i)).extract_first().strip()
                except:
                    None
                station['fuels'].append(fuel)
                
        if station != {}:
            self.data.append(station)
