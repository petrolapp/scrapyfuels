# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from parsing.basespider import BaseSpider
from scrapy import Spider, Selector, Request, FormRequest
from scrapy.http.headers import Headers
from scrapy.utils.markup import remove_tags
import json

class EnvikutHuSpider(BaseSpider):
    name = 'stations_HU_01'
    start_urls = ['http://www.envikut.hu/gasstation/Our+stations+and+prices/']
    currency = 'HUF'
    country = 'HU'

    def __init__(self):
        super(EnvikutHuSpider, self).__init__()

    def parse(self, response):
        """
        Parse site content
        """
        # Get site body text
        site_body = response.body.decode(response.encoding)
        selector = Selector(text=site_body)

        # Get all urls of gas stations
        for station in selector.css('tbody').css('a::attr(href)'):
            yield Request(
                station.extract(), dont_filter=True,
                callback=self.parse_gas_station
            )

    def parse_gas_station(self, response):
        station_data = self.create_station_data(response)
        station_data['latitude'] = None
        station_data['longitude'] = None

        # Get site body text
        site_body = response.body.decode(response.encoding)

        # Selector of gas station
        selector = Selector(text=site_body)
        station_data['name'] = selector.css('h1::text').extract_first()
        address_node = selector.xpath('//div[@class="contentarea"]/p').extract_first()
        station_data['address'] = ' '.join(
            remove_tags(address_node.split('<b>')[0], ('p', 'br')).split()
        )

        # Get fuel info
        fuel_node = selector.xpath('//h1/following-sibling::div/em/text()')
        station_data['fuels'] = [
            {
                'name': '95',
                'price': float(fuel_node[0].extract().split(' ')[0].replace(',','.'))
            },
            {
                'name': 'diesel',
                'price': float(fuel_node[1].extract().split(' ')[0].replace(',','.'))
            }
        ]
        self.data.append(station_data)

class MpetrolHuSpider(BaseSpider):
    name = 'stations_HU_02'
    start_urls = 'http://mpetrol.hu/'
    currency = 'HUF'
    country = 'HU'

    def __init__(self):
        super(MpetrolHuSpider, self).__init__()

    def start_requests(self):
        """
        Override base method that use Splash
        """
        return [Request(self.start_urls, callback=self.parse, meta={
            'splash': {'endpoint': 'render.html', 'args': {'wait': 7}}})]

    def parse(self, response):
        """
        Parse site content
        """
        # Get site body text
        site_body = response.body.decode(response.encoding)
        selector = Selector(text=site_body)

        #data = json.loads(site_body.split('totem_stations = ')[1].split(';')[0])

        # yield FormRequest of gas stations
        url = '{}ajax/getjsdata.php'.format(self.start_urls)
        headers = Headers(
            {'Content-Type': 'application/x-www-form-urlencoded'}
        )
        for val in selector.xpath('//select/option/@value'):
            yield FormRequest(
                url=url, headers=headers, method='post',
                formdata={
                    'datatype': 'GETGASTATIONDATA', 'gasid': val.extract()
                },
                callback=self.parse_gas_station
            )

    def parse_gas_station(self, response):
        """
        Parse gas station site content
        """
        station_data = self.create_station_data(response)
        print response.body
        data = json.loads(response.body)['data']
        station_data['latitude'] = None
        station_data['longitude'] = None

        def price(string):
            return float('{}.{}'.format(string[:-1], string[-1])) if (len(string) > 1) else 0.0

        station_data['fuels'] = [
            {
                'name': 'e85',
                'price': price(data.get('e85'))
            },
            {
                'name': 'b95',
                'price':  price(data.get('b95'))
            },
            {
                'name': 'b98',
                'price':  price(data.get('b98'))
            },
            {
                'name': 'diesel',
                'price':  price(data.get('diesel'))
            },
            {
                'name': 'lpg',
                'price':  price(data.get('lpg'))
            }
        ]
        station_data['name'] = data['city']
        station_data['address'] = data['address']
        self.data.append(station_data)