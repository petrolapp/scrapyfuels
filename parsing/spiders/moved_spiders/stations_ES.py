import time
from datetime import datetime
from scrapy import Spider, Request, Selector
from parsing.basespider import BaseSpider

class SE_Spider(BaseSpider):
    name = 'stations_es'
    allowed_domains = ['dieselogasolina.com']
    start_urls = ['http://www.dieselogasolina.com/Buscador/Ficha/']
    base_url = 'http://www.dieselogasolina.com/Buscador/Ficha/{0}'

    country = 'ES'
    currency = 'EUR'

    counter_410 = 0
    # xpath
    last_updated_xpath = '//*[@class="contenido_cubo"]/p/b/text()'

    # name, city, province, street
    info_block_path = '//*[@class="cubo"]/h1/text()'    


    map_xpath = '//*[@id="map_canvas"]/img/@src'

    petrol_block_xpath = '//*[@id="ultimos_precios"]/li'
    # old prices xpath
    old_table_xpath = '//*[@id="precios_anteriores"]/table'
    old_name_xpath = '//table/tbody/tr/th/text()'
    old_price_xpath = '//table/tbody/tr/td[1]/text()'

        # old info xpath
    old_info_block_xpath = '//*[@id="busqueda"]'
    old_last_updated_xpath = '//*[@id="fecha_ultima_actualizacion"]/text()'

    def __init__(self):
        super(SE_Spider, self).__init__()
        print self.name

    def parse(self, response):
        for i in range(1, 15561):
            yield Request(url=self.base_url.format(i), callback=self.parse_station, dont_filter=True)

    def parse_station(self, response):
        xhs = Selector(response)
        loc_index = response.body.find('var point_tmp = new google.maps.LatLng(')

        # if this page do not contains map - it do not contains station
        if loc_index == -1:
            return
        lat_and_lot = response.body[loc_index:].split('\n')[0].split('(')[1].split(')')[0]

        # if this page do not contains map - it do not contains station
        #if not xhs.xpath(self.map_xpath).extract():
        #    return

        # from map_url we could extract coordinates example:
        # http://maps.googleapis.com/maps/api/staticmap?center=39.211417,-1.539167&zoom=15&size=450x399&markers=39.211417,-1.539167&sensor=true
        #map_url = xhs.xpath(self.map_xpath).extract()[0].strip()

        info = xhs.xpath(self.info_block_path).extract()[0] 

        station = self.create_station_data(response)
        #station['iframe_url'] = map_url

        station['latitude'] = float(lat_and_lot.split(',')[0].strip())
        station['longitude'] = float(lat_and_lot.split(',')[1].split('&')[0].strip())
        station['name'] = info.split('Gasolinera ')[1].split(' en ')[0].strip()
        station['street'] = info.split('>')[1].strip()

        station['city'] = info.split(' en ')[1].split('(')[0].strip()
        station['province'] = info.split('>')[0][info.split('>')[0].rfind('(')+1:].split(')')[0].strip()

        petrols = []

        date_str = xhs.xpath(self.last_updated_xpath).extract()[0]
        #date_str = xhs.xpath(self.last_updated_xpath).extract()[0].strip()
        #last_updated = datetime.fromtimestamp(time.mktime(time.strptime(date_str, '%d/%m/%Y')))
        for petrol_block in xhs.xpath(self.petrol_block_xpath).extract():
            petrol = Selector(text=petrol_block).xpath('//li/span/text()').extract()
            price = float(petrol[1].split(' ')[0].strip().replace(',',  '.'))
            name = petrol[0].strip()
            petrols.append({'name': name, 'price': price, 'last_updated': date_str})
        # in some cases main block with prices is empty
        # its usual if last updating of prices was long ago
        if not petrols:
            for block in xhs.xpath(self.old_table_xpath).extract():
                sel = Selector(text=block)
                name = sel.xpath(self.old_name_xpath).extract()[0].strip()
                price = float(sel.xpath(self.old_price_xpath).extract()[0].strip())
                petrols.append({'name': name, 'price': price, 'last_updated': date_str})
        station['fuels'] = petrols
        self.data.append(station)