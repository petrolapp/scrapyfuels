import time
from datetime import datetime
from scrapy import Spider, Request, Selector
from parsing.basespider import BaseSpider


class BE_FR_LU_Spider(BaseSpider):
    name = 'stations_be_fr_lu'
    currency = 'EUR'
    allowed_domains = ['carbu.vroom.be']
    start_urls = ['http://carbu.vroom.be//index.php/meilleurs-prix/Luxembourg/LU/0',
            'http://carbu.vroom.be//index.php/meilleurs-prix/Belgique/BE/0',
            'http://carbu.vroom.be//index.php/meilleurs-prix/France/FR/0']
    parser_countries = ['BE', 'FR', 'LU']
    # xpath
    province_xpath = '//a[@class="floatLeft"]/@href'
    stations_block_xpath = '//*[contains(@id, "item_")]'
    station_url_xpath = '//a/@href'
    station_name_xpath = '//*[@itemprop="name"]/text()'
    street_zip_xpath = '//*[@itemprop="street-address"]/text()'
    city_xpath = '//*[@itemprop="locality"]/text()'
    petrol_panel_xpath = '//*[@class="panel panel-primary"]'
    petrol_name_xpath = '//h1/text()'
    petrol_price_xpath = '//div/div[2]/h1/text()'
    petrol_date_xpath = '//div/div[2]/sub[1]/text()'

    def parse(self, response):
        xhs = Selector(response)
        provinces = xhs.xpath(self.province_xpath).extract()
        for province in provinces:
            yield Request(url=province, callback=self.parse_province, dont_filter=True)

    def parse_province(self, response):
        xhs = Selector(response)
        cities = xhs.xpath(self.province_xpath).extract()
        for city in cities:
            yield Request(url=city, callback=self.parse_city, dont_filter=True)

    def parse_city(self, response):
        xhs = Selector(response)
        stations = xhs.xpath(self.stations_block_xpath).extract()
        for station in stations:
            station_url = Selector(text=station).xpath(self.station_url_xpath).extract()[0].strip()
            yield Request(url=station_url, callback=self.parse_station, dont_filter=True)
        if not stations:
            urls = xhs.xpath(self.province_xpath).extract()
            for next_url in urls:
                yield Request(url=next_url, callback=self.parse_province, dont_filter=True)

    def parse_station(self, response):
        xhs = Selector(response)
        country = response.request.headers.get('Referer', None).split('/')[-1].split('_')[0]
        station = self.create_station_data(response)
        station['country'] = country
        # station could be without name: http://carbu.vroom.be//index.php/station/match/longwy/54400/15585
        name = xhs.xpath(self.station_name_xpath).extract()
        if name:
            station['name'] = name[0].strip()
        street_zip = xhs.xpath(self.street_zip_xpath).extract()
        station['street'] = street_zip[0].strip()
        station['zip'] = street_zip[1].strip()
        station['city'] = xhs.xpath(self.city_xpath).extract()[0].strip()
        gps = response.body.split('centerLat =')[1]
        station['latitude'] = float(gps.split(';')[0].strip())
        station['longitude'] = float(gps.split('centerLng =')[1].split(';')[0].strip())
        petrol_panels = xhs.xpath(self.petrol_panel_xpath).extract()
        fuels = []
        for panel in petrol_panels:
            sel = Selector(text=panel)
            petrol_name = sel.xpath(self.petrol_name_xpath).extract()[0].strip()
            date_ = sel.xpath(self.petrol_date_xpath).extract()
            if date_:
                fuel = {'name': petrol_name}
                date_str = date_[0].strip()
                fuel['last_updated'] = str(datetime.fromtimestamp(time.mktime(time.strptime(date_str, '%Y-%m-%d %X'))))
                fuel['price'] = float(sel.xpath(self.petrol_price_xpath).extract()[0].split(' ')[0].strip().replace(',', '.'))
                fuels.append(fuel)
        station['fuels'] = fuels
        self.data.append(station)

    def save_page(self, content, number):
        with open('page_' + str(number) + '.html', 'wb') as f:
            f.write(content)
