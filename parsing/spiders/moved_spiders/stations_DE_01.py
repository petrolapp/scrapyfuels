import time
from datetime import datetime
from scrapy import Spider, Request, Selector
from parsing.basespider import BaseSpider


class DE_Spider_01(BaseSpider):
    name = 'stations_de_01'
    country = 'DE'
    currency = 'EUR'
    allowed_domains = ['clever-tanken.de']
    start_urls = ['http://www.clever-tanken.de/statistik/staedte']
    base_url = 'http://www.clever-tanken.de'
    city_results_url = 'http://www.clever-tanken.de/tankstelle_liste?spritsorte=3&ort='
    # xpath
    city_list_xpath = '//*[@id="price_records_cities"]/tbody/tr'
    city_xpath = '//tr/td[1]/a/text()'
    stations_row_xpath = '//*[@id="main-content-fuel-station-list"]/a/@href'
    station_name_xpath = '//*[@itemprop="name"]/text()'
    street_xpath = '//*[@itemprop="streetAddress"]/text()'
    zip_xpath = '//*[@itemprop="http://schema.org/postalCode"]/text()'
    city_name_xpath = '//*[@itemprop="http://schema.org/addressCountry"]/text()'
    geo_xpath = '//*[@id="main-content-fuel-station-location-block"]/a/@href'
    fuel_block_xpath = '//div[@ng-controller="SpritsortenController"]'
    price_xpath = '//div/@ng-init'
    last_updated_block = '//div[@class="fuel-price-header"]/text()[1]'
    petrol_name_xpath = '//div/span[1]/text()'
    pagination_xpath = '//*[@class="page-current"]/text()'

    def parse(self, response):
        xhs = Selector(response)
        cities_blocks = xhs.xpath(self.city_list_xpath).extract()
        for city_block in cities_blocks[1:]:
            sel = Selector(text=city_block)
            part = sel.xpath(self.city_xpath).extract()
            city_results_url = self.city_results_url + part[0]
            yield Request(url=city_results_url, callback=self.parse_city, dont_filter=True)

    def parse_city(self, response):
        xhs = Selector(response)
        stations = xhs.xpath(self.stations_row_xpath).extract()
        for station in stations:
            station_url = self.base_url + station
            yield Request(url=station_url, callback=self.parse_station, dont_filter=True)
        pages = xhs.xpath(self.pagination_xpath).extract()
        if pages:
            amount = int(pages[0].split("von ")[1].strip())
            if '&page=' in response.url:
                current = int(response.url.split('page=')[1].strip())
                if current < amount:
                    next_page_url = response.url.replace('page=' + str(current), 'page=' + str(current + 1))
                    yield Request(url=next_page_url, callback=self.parse_city, dont_filter=True)
            else:
                yield Request(url=response.url + '&page=2', callback=self.parse_city, dont_filter=True)

    def parse_station(self, response):
        xhs = Selector(response)
        station = self.create_station_data(response)
        station['name'] = xhs.xpath(self.station_name_xpath).extract()[0].strip()
        station['street'] = xhs.xpath(self.street_xpath).extract()[0].strip()
        station['zip'] = xhs.xpath(self.zip_xpath).extract()[0].strip()
        station['city'] = xhs.xpath(self.city_name_xpath).extract()[0].strip()
        geo = xhs.xpath(self.geo_xpath).extract()[0].strip()
        station['latitude'] = float(geo.split('q=')[1].split(',')[0].strip())
        station['longitude'] = float(geo.split('q=')[1].split(',')[1].strip())
        station['url'] = response.url
        last_updated = None
        last_updated_block = xhs.xpath(self.last_updated_block).extract()
        if last_updated_block:
            date_str = last_updated_block[0].split(": ")[1].split(' ')[0].strip()
            # format: 05.08.2015
            last_updated = str(datetime.fromtimestamp(time.mktime(time.strptime(date_str, '%d.%m.%Y'))))
        fuels = xhs.xpath(self.fuel_block_xpath).extract()
        petrols = []
        for fuel in fuels:
            sel = Selector(text=fuel)
            price_str = sel.xpath(self.price_xpath).extract()[0].split("'")[1].split("'")[0]
            if price_str == 'None':
                continue
            petrol= {}
            petrol['price'] = float(price_str.replace(',', '.'))
            petrol['last_updated'] = last_updated
            petrol['name'] = sel.xpath(self.petrol_name_xpath).extract()[0].strip()
            petrols.append(petrol)
        station['fuels'] = petrols
        self.data.append(station)
