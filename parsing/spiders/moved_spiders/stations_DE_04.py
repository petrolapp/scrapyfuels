#!/usr/bin/env python
#coding: utf8

import re
import json
from datetime import datetime
from scrapy import Spider
from parsing.basespider import BaseSpider


"""
This site returns huge json.
Parameter 'r' in request means radius.
Need to add a few new point in Germany to cover
all area in a case of radius restrictions on server.
"""

class DE_Spider_04(BaseSpider):
    name = 'stations_de_04'
    country = 'DE'
    currency = 'EUR'
    allowed_domains = ['tanke-guenstig.de']
    start_urls = ['https://www.tanke-guenstig.de/Benzinpreise/ajax/area?c=48.135125%2C11.581981&r=5000']

    def parse(self, response):
        obj = json.loads(response.body)
        stations = obj['d']
        for st in stations:
            station = self.create_station_data(response)
            addr = st['addr']
            station['city'] = addr['city']
            station['street'] = addr['street'] + (addr['streetNumber'] if addr['streetNumber'] else '')
            station['zip'] = addr['zipCode']
            station['latitude'] = float(st['ll']['lat'])
            station['longitude'] = float(st['ll']['lng'])
            station['name'] = st['br']
            petrols = []
            pr = st['pr']
            for petrol_name in pr:
                if pr[petrol_name] and re.match("^\d+?\.\d+?$", pr[petrol_name]):
                    petrols.append({'price': float(pr[petrol_name]), 'name': petrol_name, 'last_updated': str(datetime.now())})
                else:
                    continue
            if not petrols:
                continue
            station['fuels'] = petrols
            working_hours = [None] * 7
            wh = st['ot']
            for record in wh:
                hours = record[-1]
                if not hours:
                    continue
                start = str(datetime.strptime(hours[0][0], '%H:%M'))
                end = str(datetime.strptime(hours[0][1], '%H:%M'))
                h = {'start': start, 'end': end}
                if record[0] == 'ph':
                    working_hours.append({'public_holidays': h})
                    continue
                if not record[1]:
                    working_hours[record[0] - 1] = h
                    continue
                for i in range(record[0] - 1, record[1]):
                    working_hours[i] = h
            station['working_hours'] = working_hours
            self.data.append(station)