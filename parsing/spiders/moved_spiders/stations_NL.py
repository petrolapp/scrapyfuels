import time
from datetime import datetime
import locale
from scrapy import Spider, Request, Selector
from parsing.basespider import BaseSpider

"""

Before run this spider, make sure that your system contains locale for Netherlands
to install it - run
sudo apt-get install language-pack-nl

"""
class NL_Spider(BaseSpider):
    name = 'stations_nl'
    country = 'NL'
    currency = 'EUR'
    allowed_domains = ['brandstof-zoeker.nl']
    start_urls = ['http://www.brandstof-zoeker.nl/station/']
    base_url = 'http://www.brandstof-zoeker.nl'
    # xpath
    stations_page_xpath = '//*[@id="start"]/div[1]/div/a[{0}]/@href' # 3 - 32
    station_page_xpath = '//*[@id="start"]/div[1]/div/a'
    station_street_xpath = '//*[@id="summary"]/div[1]'
    station_name_xpath = '//*[@id="summary"]/div[1]/text()[2]'
    station_zip_xpath = '//*[@id="summary"]/div[1]/a[1]/text()'
    station_city_xpath = '//*[@id="summary"]/div[1]/a[2]/text()'
    petrol_types_amount_xpath = '//*[@id="summary"]/div[2]/dl/dt'
    petrol_name_xpath = '//*[@id="summary"]/div[2]/dl/dt[{0}]/text()'
    petrol_price_date_xpath = '//*[@id="summary"]/div[2]/dl/dd[{0}]/text()[1]'

    def parse(self, response):
        locale.setlocale(locale.LC_ALL, ('nl_NL', 'utf8@euro'))
        for i in range(3, 32):
            xhs = Selector(response)
            url = self.start_urls[0] + xhs.xpath(self.stations_page_xpath.format(i)).extract()[0].split('/')[2]
            yield Request(callback=self.parse_stations_page, url=url, method='GET', dont_filter=True)

    def parse_stations_page(self, response):
        xhs = Selector(response)
        links = xhs.xpath(self.station_page_xpath).extract()
        for link in links:
            if '_blank' in link:
                continue
            station_link = Selector(text=link).xpath('//@href').extract()[0]
            url = self.base_url + station_link
            yield Request(callback=self.parse_station, url=url, method='GET', dont_filter=True)

    def parse_station(self, response):
        xhs = Selector(response)
        coordinates = response.body.split('LatLng')[1].split(')')[0].split('(')[1]
        station = self.create_station_data(response)
        station['name'] = xhs.xpath(self.station_name_xpath).extract()[0].strip()
        station['zip'] = xhs.xpath(self.station_zip_xpath).extract()[0].strip()
        station['street'] = xhs.xpath(self.station_street_xpath).extract()[0].split('<br>')[1].strip()
        station['city'] = xhs.xpath(self.station_city_xpath).extract()[0].strip()
        station['latitude'] = float(coordinates.split(',')[0].strip())
        station['longitude'] = float(coordinates.split(',')[1].strip())
        station['url'] = response.url
        petrols = []
        for i in range(1, len(xhs.xpath(self.petrol_types_amount_xpath).extract()) + 1):
            petrol_name = xhs.xpath(self.petrol_name_xpath.format(i)).extract()[0].strip()
            # string looks like E 1.650 (6 juli 2015)
            price_date_block = xhs.xpath(self.petrol_price_date_xpath.format(i)).extract()[0]
            price = float(price_date_block.split('(')[0].split(' ')[1])
            nl_date = price_date_block.split('(')[1].split(')')[0]
            last_updated = str(datetime.fromtimestamp(time.mktime(time.strptime(nl_date, '%d %B %Y'))))
            petrols.append({'name': petrol_name, 'price': price, 'last_updated': last_updated})
        station['fuels'] = petrols
        self.data.append(station)
