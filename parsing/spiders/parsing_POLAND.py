# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from parsing.basespider import BaseSpider
from scrapy import Spider, Selector, Request
from scrapy.utils.markup import remove_tags
from slugify import slugify

class PolandSpider(BaseSpider):
    name = 'stations_PL'
    start_urls = ['http://www.stacjebenzynowe.pl/search_stacje.php']
    country = 'PL'
    currency = 'PLN'

    def __init__(self):
        super(PolandSpider, self).__init__()

    def parse(self, response):
        """
        Parse provinces page
        """
        # Get site body text
        site_body = response.body.decode(response.encoding)

        # Get list provinces Poland
        selector = Selector(text=site_body)
        provinces = selector.xpath('//select[@id="searchstacja_woj"]/option')

        for province in provinces[1:]:
            yield Request(
                '{0}?action=srch&searchstacja_woj={1}'.format(
                    self.start_urls[0].split('?')[0],
                    province.xpath('@value').extract_first()
                ),
                callback=self.parse_province, dont_filter=True
            )

    def parse_province(self, response):
        # Get site body text
        site_body = self.convert_body(response)

        # Get list gas stations of city
        selector = Selector(text=site_body)
        fuels_selector = selector.xpath('//td/table[count(tr)>10]')
        fuels_name = [
            slugify(
                name.css('b::text').extract_first().split()[1]
            ) for name in fuels_selector.xpath('tr[1]/td')[1:]
        ]
        for fuel in fuels_selector.css('tr')[2:]:
            station_data = self.create_station_data(response)
            station_data['url'] = '{}/{}'.format(self.domain,
                fuel.css('td::attr(onclick)')[1].extract().split("'")[-2])
            station_data['latitude'] = None
            station_data['longitude'] = None

            # Selector of gas station
            station_data['name'] = remove_tags(fuel.css('td')[1].extract(), 'td')
            station_data['address'] = station_data['name'].split('-')[-1].strip()

            # Get fuel info
            for i, price_td in enumerate(fuel.css('td')[2:]):
                price = price_td.css('b::text').extract_first()
                last_updated = None
                if price is None:
                    price = price_td.css('font::text').extract_first()
                else:
                    last_updated = price_td.css('td::attr(onmouseover)').extract_first().split(': ')[1].split(' <')[0]
                if price is None:
                    continue
                station_data['fuels'].append(
                    {
                        'name': fuels_name[i],
                        'price': float(price.split()[0].replace(',','.')),
                        'last_updated': last_updated
                    }
                )
            self.data.append(station_data)