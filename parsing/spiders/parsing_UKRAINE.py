# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from parsing.basespider import BaseSpider
from scrapy import Spider, Selector, Request
from slugify import slugify

class UkraineSpider(BaseSpider):
    name = 'stations_UA'
    start_urls = ['http://oilcard.wog.ua/ukr/azs/']
    country = 'UA'
    currency = 'UAH'

    def __init__(self):
        super(UkraineSpider, self).__init__()

    def parse(self, response):
        """
        Parse site content
        """
        # Get site body text
        site_body = response.body.decode(response.encoding)

        # Use XPath
        selector = Selector(text=site_body)

        # Get list regions
        regions_selector = selector.xpath('//div[@class="table hover region text-center"]')[0]
        regions_link = [s.css('a::attr(href)').extract_first() for s in regions_selector.css('a')]
        regions_link = dict.fromkeys(regions_link).keys()

        for link in regions_link:
            yield Request(
                '{0}{1}'.format(self.domain, link),
                callback=self.parse_gas_stations, dont_filter=True
            )

    def parse_gas_stations(self, response):
        # Get site body text
        site_body = response.body.decode(response.encoding)

        # Get list cities of region
        selector = Selector(text=site_body)

        for station in selector.xpath('//div[@class="table hover stations"]/div'):
            yield Request(
                '{0}{1}'.format(
                    self.domain, station.css('a::attr(href)').extract_first()
                ), callback=self.parse_gas_station, dont_filter=True
            )

    def parse_gas_station(self, response):
        station_data = self.create_station_data(response)
        # Get site body text
        site_body = response.body.decode(response.encoding)

        # Selector of gas station
        gas_station_selector = Selector(text=site_body)

        station_data['latitude'] = None
        station_data['longitude'] = None
        geo = gas_station_selector.xpath('//iframe/@src').extract()
        if len(geo) > 0:
            geo_arr = geo[0].split('&q=')[1].split('&')[0].split(',')
            station_data['latitude'] = float(geo_arr[0])
            station_data['longitude'] = float(geo_arr[1])

        info = gas_station_selector.xpath('//div[@class="table"]//b/text()')
        station_data['name'] = info[0].extract(),
        station_data['address'] = info[1].extract()

        # Get fuel info
        fuels_selector = gas_station_selector.xpath('//div[@class="table price w30 fleft"]/div')

        if len(fuels_selector) > 2:
            last_updated = fuels_selector[-1].xpath('text()').extract_first()
            #print last_updated
            for fuel in fuels_selector[1:-2]:
                fuel_sel = fuel.xpath('span/text()')
                station_data['fuels'].append({
                    'name': slugify(fuel_sel[0].extract()),
                    'price': float(fuel_sel[1].extract()),
                    'last_updated': last_updated
                })
            self.data.append(station_data)