# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from urlparse import urlparse
from scrapy import Spider, Selector, Request
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
from slugify import slugify
from parsing.basespider import BaseSpider
from selenium import webdriver

import json
import datetime


class UkraineSpider(BaseSpider):
    name = 'stations_UA_02'
    start_urls = ['http://azs.uapetrol.com/window/azsInfo.html']
    base_url = 'http://azs.uapetrol.com/window/azsInfo.html?azs={0}'
    country = 'UA'
    currency = 'UAH'

    def __init__(self):
        super(UkraineSpider, self).__init__()

    def parse(self, response):
        for i in range(2, 5994):
            yield Request(url=self.base_url.format(i), callback=self.parse_station, dont_filter=True,
                meta={ 'splash': { 'endpoint': 'render.html', 'args': {'wait': 3 }}})

    def parse_station(self, response):
        station = self.create_station_data(response)
        station['url'] = response.meta['_splash_processed']['args']['url']
        source = self.convert_body(response)
        xhs = Selector(text=source)

        coord = xhs.xpath('//*[@class="gm-style"]//*[contains(@href, "maps.google.com")]/@href').extract()[0]
        coord = coord.split('=')[1].split('&')[0]
        station['latitude'] = float(coord.split(',')[0])
        station['longitude'] = float(coord.split(',')[1])

        address = xhs.xpath('//*[@id="azsAddr"]/text()').extract()[0]
        station['address'] = address

        station_name = xhs.xpath('//body/table/tbody/tr/td[3]/table/tbody/tr[1]/td/text()').extract()[0]
        station['name'] = station_name

        fuels = xhs.xpath('//*[@id="brendStella"]/table/tbody/tr').extract()
        for f in fuels:
            sel = Selector(text=f)
            name = sel.xpath('//*[contains(@tooltip, "")]/@tooltip').extract()
            if len(name) == 0:
                continue
            fuel = {}
            fuel['name'] = name[0]
            fuel['price'] = float(sel.xpath('//*[@class="fuelPrice"]/text()').extract()[0])
            station['fuels'].append(fuel)
        self.data.append(station)