# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import re
import requests
import scrapy
import datetime
import time


FUEL_TYPES = {
    u'Super 95': 'fuel_a95',
    u'Super': 'fuel_a95',
    u'Normal': 'fuel_a95',
    u'Diesel': 'fuel_diesel',
    u'LPG': 'fuel_autogas',
    u'Autogas (LPG)': 'fuel_autogas',
    u'Super Plus': 'fuel_a98',
    u'Premium Benzin': 'fuel_a98',
    u'Premium Diesel': 'fuel_dieselsp',
    u'Biodiesel': 'fuel_biodiesel',
    u'strom': 'fuel_electro',
    u'Ethanol': 'fuel_ethanol',
    u'Erdgas (CNG)': 'fuel_egas',

}

CURRENCY = 'EUR'


class OemTcSpider(scrapy.Spider):
    name = 'oeamtc_AUT'
    start_urls = ['http://www.oeamtc.at/spritapp/SimpleSearch.do?spritaction=showSimple']
    custom_settings = {'BOT_NAME': 'Yandex Parse Bot',
                       'DOWNLOAD_DELAY': 1.5,
                       'CONCURRENT_REQUESTS': 10}

    def parse(self, response):
        """
        Parse site content
        """
        domain_url = 'http://www.oeamtc.at'

        def _parse_fuel_blocks(fuel_type, list_blocks):
            """
            Parse fuel type, price and pub_date.
            """
            res_data = dict()
            st_longitude = None
            st_latitude = None

            for row_block in list_blocks:

                # Get fuel price
                fuel_price_lst = row_block.xpath("*[@class='fuelPrice']/text()").re('[^\s]+')

                # If empty row data - break
                if fuel_price_lst:
                    fuel_price = fuel_price_lst[0]
                else:
                    break

                # Get stationName
                st_name = row_block.xpath("*[@class='stationName']//a/text()").extract_first().strip()

                # Get address
                addr_list = list()
                addr_raw_list = row_block.xpath("*[@class='address']//text()").extract()

                for addr_row in addr_raw_list:
                    if addr_row.strip():
                        addr_list.append(addr_row.strip())
                # Create address string
                st_address = ', '.join(addr_list)

                # Get pub date
                pub_date = ' '.join(row_block.xpath("*[@class='reportTimestamp']//text()").re('[^\s]+')).replace(
                    'heute', datetime.datetime.now().strftime('%F %T'))

                # Init res_data
                res_data['addresses'] = res_data.get('addresses', dict())
                res_data['addresses'][st_address] = res_data['addresses'].get(st_address, dict())

                # Add fuel data
                res_data['addresses'][st_address].update({fuel_type: dict(price=fuel_price, pub_date=pub_date),
                                                          'name': st_name,
                                                          'longitude': st_longitude,
                                                          'latitude': st_latitude})

            return res_data

        # Match count
        match_count = re.compile(r'[^\(]+\((?P<num>[0-9]+).*?')

        # Default fuel data
        st_url = response.url

        # Default fuel data
        fuel_data = dict(status=True,  addresses=dict(), url=st_url, currency=CURRENCY)

        # Request params
        req_data = dict(selectedFuelType='diesel', sortBy='price', state='', ZIP='', ResultPageSize='50',
                        spritaction='doSimpleSearch')

        try:
            # Setup selector
            selector = scrapy.Selector(text=response.body.decode(response.encoding))

            # Get all fuel option
            fuel_option = dict()
            fuel_option_blocks = selector.xpath('.//*[@id="selectedFuelType"]/option')

            # Save all fuel option
            for option in fuel_option_blocks:
                fuel_key = option.xpath('@value').extract_first().strip()
                fuel_name = option.xpath('text()').extract_first()
                fuel_option[fuel_key] = fuel_name

            # Get fuel type information and price
            for fuel_tp in list(fuel_option.keys()):

                # Setup request data
                req_data['selectedFuelType'] = fuel_tp.strip()

                # Setup POST data
                post_data = req_data

                if fuel_tp != 'diesel' or fuel_tp != 'super':
                    post_data['daysLimit'] = 'today'

                fuel_type = FUEL_TYPES.get(fuel_option[fuel_tp], None) or fuel_tp

                # Make request and get fuel_type prices
                post_url = '%s%s' % (domain_url, '/spritapp/SimpleSearch.do')
                req = requests.post(post_url, data=post_data)
                time.sleep(3)

                # Check status
                if req.status_code != 200:
                    continue

                # Parse results
                sel = scrapy.Selector(text=req.content.decode(req.encoding))

                # Get max row count
                count_str = sel.xpath(".//*[@class='searchresults_header']/text()").extract_first()

                if count_str:
                    match_count_res = match_count.search(count_str.strip())
                else:
                    continue

                if match_count_res:
                    max_row_num = int(match_count_res.group('num'))

                    # Get all row on a page
                    row_blocks = sel.xpath('.//table[@class="searchResultsTable"]//tr[@class="searchResultRow"]')

                    # Get first row number and parse first page
                    if row_blocks:
                        parse_res = _parse_fuel_blocks(fuel_type, row_blocks)
                        fuel_data.update(parse_res)
                        first_row_number = int(row_blocks[0].xpath("*[@class='stationNo']/text()").re('[0-9]+')[0])

                        # Parse next pages
                        while first_row_number < max_row_num:

                            # Get next page URL
                            next_url_str = sel.xpath(
                                ".//*[@class='boxContent']/div[@class='blockrow']/div[2]/a/@href").extract_first()

                            # GET URL string
                            if next_url_str:
                                get_url = '%s%s' % (domain_url, next_url_str.strip())
                            else:
                                break

                            # Make request (get next page content)
                            next_page_req = requests.get(get_url)
                            time.sleep(3)

                            # Check status
                            if next_page_req.status_code != 200:
                                break

                            # Setup selector
                            sel = scrapy.Selector(text=next_page_req.content.decode(next_page_req.encoding))

                            # Get all row on a page
                            row_blocks = sel.xpath(
                                './/table[@class="searchResultsTable"]//tr[@class="searchResultRow"]')

                            # Get first row number
                            if row_blocks:
                                parse_res = _parse_fuel_blocks(fuel_type, row_blocks)
                                fuel_data.update(parse_res)
                                first_row_number = int(
                                    row_blocks[0].xpath("*[@class='stationNo']/text()").re('[0-9]+')[0])
                            else:
                                break

                            # Add some delay
                            time.sleep(3)

        except Exception:
            fuel_data['status'] = False

        yield fuel_data
