# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ParsingItem(scrapy.Item):
    name = scrapy.Field()
    url = scrapy.Field()
    address = scrapy.Field()
    longitude = scrapy.Field()
    latitude = scrapy.Field()
    fuels = scrapy.Field()
    country = scrapy.Field()
    currency = scrapy.Field()
    pub_date = scrapy.Field()

    __init__(self, country, currency):
        super(ParsingItem, self).__init__()
        self.country = country
        self.currency = currency