# Install PhantomJS [OPTIONAL]

**IMPORTANT!**

Built binaries already included to repository:  utils/PhantomJS/bin/

Only rebuild if you have issues with already built version on your platform!

Holds Qt libraries inside so building process can take hours!

Installation process:

*   On RH:
```yum -y install gcc gcc-c++ make flex bison gperf openssl-devel freetype-devel fontconfig-devel libicu-devel sqlite-devel libpng-devel libjpeg-devel```

*   On Ubuntu:
```sudo apt-get install build-essential g++ flex bison gperf ruby perl libsqlite3-dev libfontconfig1-dev libicu-dev libfreetype6 libssl-dev libpng-dev```
```sudo apt-get install libjpeg-dev python libx11-dev libxext-dev```

```python
git clone git://github.com/ariya/phantomjs.git
cd phantomjs
git checkout 2.0
./build.sh
```


# Install

git clone https://<user-name>@bitbucket.org/petrolapp/scrapyfuels.git

# Install requirements
*   On Ubuntu:
```sudo apt-get install libxslt1-dev python-dev```

With root priviledges:

```pip install -r requirements/base.txt```

# Install docker
1. Install docker: ```sudo apt-get install docker.io```
2. Run splash: ```sudo docker run -p 8050:8050 scrapinghub/splash -v3```
    *   splash run on url: ```http://localhost:8050/```
3. Command for scrapy

Note: run this commands in terminal from "scrapyfuels" root folder

*   get list spiders: ```scrapy list```
*   run spider: ```scrapy crawl name_spider```

# Run in background
To run commands in background without need to have opened session you can use screen.

*  screen - to open new screen
*  cntrl-a d - to detach from screen
*  screen -r - to reattach to screen
*  exit - to close screen (when attached)